import React, {Component} from 'react';
import Clock from './Clock';
import './App.css';

class App extends Component {
    /**
     * Constructor for the class App
     */
    constructor() {
        super();
        this.state = {
            deadline: 'December 25, 2020',
            newDeadline: '',
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    /**
     * This method will be the one responsible
     * of doing the necesary changes when the 
     * form is submit.
     */
    handleSubmit(event) {
        event.preventDefault();
        this.setState({
            deadline: this.state.newDeadline,
        });
    }
    /**
     * This method will be the one responsible
     * of doing the necesary changes when the 
     * form is submit.
     */
    handleChange(event) {
        const {name, value} = event.target
        this.setState({
            [name]: value
        });
    }
    /**
     * Render method for the class App.
     * @return {html}
     */
    render () {
        return(
            <div className='App'>
                <h1 className='App-title'>Countdown to {this.state.deadline}</h1>
                <Clock deadline={this.state.deadline} />
                <form onSubmit={this.handleSubmit}>
                    <input name='newDeadline'
                           type='text' 
                           placeholder='Input new date' 
                           value={this.state.newDeadline}
                           onChange={this.handleChange}/>
                    <button>Submit</button>
                </form>
            </div>
        );
    }
}
export default App;