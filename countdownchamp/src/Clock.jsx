import React, {Component} from 'react';
import './App.css'

class Clock extends Component {
    /**
     * Constructor for the class clock.
     */
    constructor() {
        super();
        this.state = {
            days: 0,
            hours: 0,
            minutes: 0,
            seconds: 0,
        };
    }
    /**
     * This is a lifecycle method will trigger before the
     * component is rendered.
     */
    componentWillMount() {
        this.getTimeUntil(this.props.deadline);
    }
    /**
     * This is a lifecycle method that will trigger
     * after the component has been rendered and 
     * call a function in a certain interval
     */
    componentDidMount() {
        setInterval(() => this.getTimeUntil(this.props.deadline),1000)
    }
    /**
     * This method will add a 0 to the time
     * if this is lower than 10.
     * @param {number} num 
     */
    leading0(num) {
        return (num < 10 ? '0' + num : num);
    }
    /**
     * This is a method that will calculate the time
     * left until we reach our deadline.
     * @param {string} props: The deadline we
     * desire to use in our operation 
     */
    getTimeUntil(deadline) {
        const time = Date.parse(deadline) - Date.parse(new Date());
        const seconds = Math.floor((time/1000)%60);
        const minutes = Math.floor((time/1000/60)%60);
        const hours = Math.floor((time/(1000*60*60))%24);
        const days = Math.floor((time/(1000*60*60*24)));

        this.setState({
            days,
            hours,
            minutes,
            seconds,
        });
    }
    /**
     * This is the render method for the class
     * Clock, it will contain the html template.
     * @return {html}
     */
    render() {
        return (
            <div className='App-clock'>
                <p>{this.leading0(this.state.days)} days</p>
                <p>{this.leading0(this.state.hours)} hours</p>
                <p>{this.leading0(this.state.minutes)} minutes</p>
                <p>{this.leading0(this.state.seconds)} seconds.</p>
            </div>
        );
    }
}
export default Clock;